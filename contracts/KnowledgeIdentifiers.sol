pragma solidity ^0.5.0;

/*
Struct Site
    id = 1
    url = http://my.repo

address => Site[]
ownerHash => new Site(1, "http://my.repo")

uint => bytes32[]
1 => 2346ad27d7568ba9896f1b7da6b5991251debdf2
  => de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3

bytes32 => string
2346ad27d7568ba9896f1b7da6b5991251debdf2 => /items/my_item
de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3 => /items/another_item*/

contract KnowledgeIdentifiers {
    uint storedData;

    struct Archive {
        string domain;
        string path;
    }

    mapping (uint256 => Archive) internal archives;
    mapping (uint256 => address) internal owners;
    mapping (address => mapping (address => bool)) internal approved;
    mapping (uint256 => bytes32) internal archiveToItemHash;
    mapping (bytes32 => uint256) internal itemHashToArchive;
    mapping (bytes32 => string) internal itemHashToValue;

    function set(uint x) public {
      storedData = x;
    }

    function setArchive(uint32 _id, string memory _domain, string memory _path) public {
        Archive memory archive = Archive({domain: "", path: ""});
        archive.domain = _domain;
        archive.path = _path;
        archives[_id] = archive;

        owners[_id] = msg.sender;
    }

    function getArchive(uint32 _id) public view returns (string memory _domain, string memory _path) {
        return (archives[_id].domain, archives[_id].path);
    }

    function setApprovalForAll(address _operator, bool _approved) external {
        approved[_operator][msg.sender] = _approved;
    }

    function isApprovedForAll(address _owner, address _operator) external view returns (bool) {
        return approved[_operator][_owner];
    }

    function setItem(uint256 _archive, bytes32 _hash, string memory _value) public {
        address owner = owners[_archive];
        require((owner == msg.sender) || (this.isApprovedForAll(owner, msg.sender)));

        archiveToItemHash[_archive] = _hash;
        itemHashToArchive[_hash] = _archive;
        itemHashToValue[_hash] = _value;
    }

    function getItemPath(bytes32 _hash) public view returns (string memory) {
        return itemHashToValue[_hash];
    }

    function resolve(bytes32 _hash) public view returns (string memory) {
        uint256 id = itemHashToArchive[_hash];
        Archive memory archive = archives[id];
        return append("http://", archive.domain, archive.path, "/", itemHashToValue[_hash]);
    }

    function append(
        string memory a,
        string memory b,
        string memory c,
        string memory d,
        string memory e) internal pure returns (string memory) {
        return string(abi.encodePacked(a, b, c, d, e));
    }
}
