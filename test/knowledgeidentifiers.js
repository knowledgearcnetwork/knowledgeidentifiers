const KnowledgeIdentifiers = artifacts.require("./KnowledgeIdentifiers.sol");
const chai = require('chai'), expect = chai.expect, should = chai.should();

contract("KnowledgeIdentifiers", accounts => {
  const domain = "demo.archive.knowledgearc.net"
  const path = "/handles"

  it("creates an instance", async () => {
    const instance = await KnowledgeIdentifiers.deployed()

    await instance.setArchive(1, domain, path)

    const archive = await instance.getArchive(1)

    archive._domain.should.equal(domain)
    archive._path.should.equal(path)
  })

  it("sets an item", async () => {
      const instance = await KnowledgeIdentifiers.deployed()

      await instance.setArchive(1, domain, path)

      const itemPath = "handle/123456789/1"
      const itemHash = web3.utils.utf8ToHex(itemPath)
      await instance.setItem(1, itemHash, itemPath)

      const returnedItemPath = await instance.getItemPath(itemHash)
      returnedItemPath.should.equal(itemPath)
  })

  it("sets approval for all", async () => {
    const instance = await KnowledgeIdentifiers.deployed()

    await instance.setArchive(1, domain, path)

    await instance.setApprovalForAll(accounts[1], true)

    const isApprovedForAll = await instance.isApprovedForAll(accounts[0], accounts[1])

    expect(isApprovedForAll).to.be.true
  })

  it("resolves an archived item", async () => {
      const instance = await KnowledgeIdentifiers.deployed()

      await instance.setArchive(1, domain, path)

      const itemPath = "123456789/1"
      const itemHash = web3.utils.utf8ToHex(itemPath)
      await instance.setItem(1, itemHash, itemPath)

      const item = await instance.resolve(itemHash)

      const expected = "http://demo.archive.knowledgearc.net/handles/handle/123456789/1"

      item.should.equal(expected)
  })
})
